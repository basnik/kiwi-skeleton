<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

\Kiwi\KiwiExtension::install($configurator);

//$configurator->setDebugMode('127.0.0.1'); // enable for your remote IP

$configurator->enableDebugger(__DIR__ . '/../log');
$configurator->setTempDirectory(__DIR__ . '/../temp');
$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
if ($_SERVER['REMOTE_ADDR'] == "127.0.0.1") {
	$configurator->addConfig(__DIR__ . '/config/config.local.neon');
} else {
	$configurator->addConfig(__DIR__ . '/config/config.prod.neon');
}

return $configurator->createContainer();

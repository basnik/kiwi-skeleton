<?php
namespace OrjPha5;

use Kiwi\Be\IBackendLayoutService;
use Nette\Application\UI\Form;
use Nette\Security\User;

class BeLayoutService implements IBackendLayoutService {

	protected $user;

	public function injectUser(User $user) {
		$this->user = $user;
	}

	public function beforeRender($presenter) {
		$presenter->template->someVarWeNeedInOurMainLayout = false;
	}
}
